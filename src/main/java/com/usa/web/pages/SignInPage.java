package com.usa.web.pages;

import com.usa.web.utils.ElementHelper;
import io.qameta.allure.Step;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class SignInPage {
    private WebDriver driver;
    private ElementHelper helper;
    private static Logger LOG = Logger.getLogger(SignInPage.class.getName());

    private static final String USERNAME = "//input[@type = 'email']";
    private static final String NEXT_BUTTON = "//div[@id ='identifierNext']";
    private static final String ERROR_MESSAGE = "//div[@class = 'GQ8Pzc']";

    public SignInPage(WebDriver driver) {
        this.driver = driver;
        helper = new ElementHelper(driver);
    }

    @Step
    public SignInPage fillInUserName(String email) {
        LOG.info("Fill form on Sign In Page");
        helper.enterText(USERNAME, email);
        helper.clickOnElement(NEXT_BUTTON);
        return this;
    }

    @Step
    public void verifyError(String expectedErrorMessage) {
        LOG.info("verify error");
        Assert.assertEquals(helper.getText(ERROR_MESSAGE), expectedErrorMessage);
    }
}
