package com.usa.web.pages;

import com.usa.web.utils.ElementHelper;
import io.qameta.allure.Step;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static com.usa.web.utils.PropertyLoader.getBaseUrl;

public class MainPage {

    private WebDriver driver;
    private ElementHelper helper;
    private static Logger LOG = Logger.getLogger(MainPage.class.getName());
    private static int WAIT_TIME = 5;


    private static final String SIGN_IN_BUTTON = "//ul[@class = 'h-c-header__cta-list header__nav--ltr']//a[contains(text(), 'Sign in')]";
    private static final String CREATE_ACCOUNT_BUTTON = "/ul[@class = 'h-c-header__cta-list header__nav--ltr']//*[contains(text(),'Create an account')]";

    public MainPage(WebDriver driver) {
        this.driver = driver;
        helper = new ElementHelper(driver);
    }

    @Step("Open main page with url : {0}")
    public MainPage open() {
        String url = getBaseUrl();
        LOG.info("Open with url: " + url);
        driver.get(url);
        return this;
    }

    @Step("Verify title: {0}")
    public void verifyTitle(String title) {
        Assert.assertTrue(driver.getTitle().equals(title));
    }

    public String getTitle() {
        return driver.getTitle();
    }

    @Step("Go to SignIn page")
    public SignInPage goToSignInPage() {
        LOG.info("Navigate to Sign In page");
        helper.clickOnElement(SIGN_IN_BUTTON);
        helper.delay(WAIT_TIME);
        helper.waitPageLoaded();
        return new SignInPage(driver);
    }

    @Step("Go to SignUP page")
    public SignUpPage goToSignUpPage() {
        LOG.info("Navigate to Sign Up page");
        helper.clickOnElement(CREATE_ACCOUNT_BUTTON);
        helper.waitPageLoaded();
        return new SignUpPage(driver);
    }
}
