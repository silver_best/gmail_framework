package com.usa.web.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.usa.web.utils.PropertyLoader.getDefaultWait;
import static java.lang.String.format;

public class ElementHelper {
    private WebDriver driver;
    private static Logger LOG = Logger.getLogger(ElementHelper.class.getName());

    public ElementHelper(WebDriver driver) {
        this.driver = driver;
    }

    public String getText(String locator) {
        return driver.findElement(getTypeLocator(locator)).getText().trim();
    }

    public void enterText(String locator, String text) {
        LOG.info(format("Clear and set text: %s to an element with locator: %s", text, locator));
        waitForElement(locator).clear();
        waitForElement(locator).sendKeys(text);
    }

    public void enterText(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void clickOnElement(String locator) {
        LOG.info("CLick on element with locator: " + locator);
        waitForElement(locator).click();
    }

    public boolean waitUntilElementDisplayed(String locator) {
        return waitUntilElementDisplayed(locator, 8);
    }

    public boolean waitUntilElementDisplayed(String locator, int timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        try {
            wait.until(driver -> driver.findElement(getTypeLocator(locator)).isDisplayed());
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public void switchToTab() {
        String currentTab = driver.getWindowHandle();

        for (String tab : driver.getWindowHandles()) {
            if (!tab.equals(currentTab)) {
                driver.switchTo().window(tab);
                driver.navigate().to(driver.getCurrentUrl());
            }
        }
    }

    private WebElement waitForElement(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, getDefaultWait());
        return wait.until(driver -> {
            WebElement element = driver.findElement(getTypeLocator(locator));
            if (element != null && element.isDisplayed()) {
                return element;
            }

            return null;
        });
    }

    public static By getTypeLocator(String locator) {
        if (locator.startsWith("//") || locator.startsWith("/")) {
            return By.xpath(locator);
        }

        return By.cssSelector(locator);
    }

    public void waitPageLoaded() {
        new WebDriverWait(driver, getDefaultWait()).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public void delay(int waitTimeInSec) {
        try {
            Thread.sleep(waitTimeInSec * 1000);
        } catch (InterruptedException e) {
            LOG.info("delay exception: " + e.getMessage());
        }
    }
}
