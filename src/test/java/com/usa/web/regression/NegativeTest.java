package com.usa.web.regression;

import com.usa.web.TestRunner;
import com.usa.web.pages.MainPage;
import com.usa.web.pages.SignInPage;
import com.usa.web.test_data.DataGenerator;
import com.usa.web.utils.ElementHelper;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class NegativeTest extends TestRunner {
    MainPage mainPage;
    ElementHelper helper;
    SignInPage signInPage;


    @BeforeMethod
    public void beforeClass() {
        mainPage = new MainPage(driver);
        helper = new ElementHelper(driver);
        signInPage = new SignInPage(driver);
    }

    @Test(dataProvider = "UserAccount", dataProviderClass = DataGenerator.class)
    public void loginWithInvalidUsernameTest(String email, String expectedErrorMessage) {
        mainPage
                .open()
                .goToSignInPage();
        helper.switchToTab();
        signInPage.fillInUserName(email)
                .verifyError(expectedErrorMessage);
    }
}
