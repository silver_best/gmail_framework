package com.usa.web.regression;

import com.usa.web.TestRunner;
import com.usa.web.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class MainPageTest extends TestRunner {

    MainPage mainPage;

    private static final String EXPECTED_TITLE = "Gmail - Free Storage and Email from Google";

    @BeforeMethod
    public void beforeClass() {
        mainPage = new MainPage(driver);
    }

    @Test
    public void verifyTitleTest() {
        mainPage.open();
        mainPage.verifyTitle(EXPECTED_TITLE);

        Assert.assertEquals(mainPage.getTitle(), EXPECTED_TITLE);
    }
}
