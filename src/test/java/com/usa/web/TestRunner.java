package com.usa.web;

import com.usa.web.listeners.LogListener;
import com.usa.web.utils.CaptureScreenshots;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.util.concurrent.TimeUnit;

import static com.usa.web.utils.PropertyLoader.getDefaultWait;


@Listeners(value = {LogListener.class})
public class TestRunner {
    protected WebDriver driver;

    @BeforeMethod
    public void beforeSetUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-fullscreen");
        options.addArguments("disable-infobars");
        driver = new ChromeDriver(options);


        driver.manage().timeouts().implicitlyWait(getDefaultWait(), TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
    }


    @AfterMethod
    public void tearDown(ITestResult result) {
        if (!result.isSuccess() && driver != null) {
            CaptureScreenshots.captureScreenByte(driver);
        }

        if (driver != null) {
            driver.quit();
        }
    }

}
